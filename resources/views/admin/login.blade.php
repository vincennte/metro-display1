<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    @vite(['resources/css/app.css','resources/js/app.js'])
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/tailwind.css')}}" />
    <title>Admin| Metro Display ADV</title>
    <link rel="shortcut icon" href="{{asset('assets/images/logo/logo-putih.png')}}" type="image/x-icon" />
    <script src="{{asset('js/tailwindcss.js')}}"></script>
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/flowbite/1.6.4/flowbite.min.js"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.js"> </script>

    <script>
        new WOW().init();
    </script>
</head>
<body>
    <section class="md:bg-[url('/img/bg.gif')] md:bg-cover bg-[url('/img/bg-1.gif')] bg-cover">
        <div class="wow fadeInUp flex flex-col items-center h-cover backdrop-brightness-50 justify-center px-6 py-8 mx-auto h-screen lg:py-0">
            @if (session()->has('loginError'))
                <div class="p-4 mb-4 text-sm text-red-800 rounded-lg bg-red-50 " role="alert">
                    {{session('loginError')}}
                </div>
            @endif
            <a class="hover:scale-[1.05] transition-all duration-500 cursor-pointer md:flex md:text-white mb-6 text-2xl font-semibold text-white ">
                <img class="w-8 h-8 m-auto" src="{{asset('assets/images/logo/logo-putih.png')}}" alt="logo">
                Admin | Metro Display ADV    
            </a>
            <div class="hover:scale-[1.05] transition-all duration-500 w-full backdrop-blur-lg bg-violet-600 rounded-lg shadow md:mt-0 sm:max-w-md xl:p-0 ">
                <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
                    <h1 class="text-xl font-bold leading-tight tracking-tight text-white md:text-2xl">
                        Sign in to your account
                    </h1>
                    <form method="POST" action="/login" class="space-y-4 md:space-y-6" >
                        @csrf
                        <div>
                            <label for="email" class="block mb-2 text-sm font-medium text-white">Your email</label>
                            <input type="email" name="email" id="email" class="bg-gray-50 border border-gray-300 text-black sm:text-sm rounded-lg focus:ring-blue-600 focus:border-blue-600 block w-full p-2.5 bg" placeholder="name@company.com" autofocus required>
                        </div>
                        <div>
                            <label for="password" class="block mb-2 text-sm font-medium text-white">Password</label>
                            <input type="password" name="password" id="password" placeholder="••••••••" class="bg-gray-50 border border-gray-300 text-black sm:text-sm rounded-lg focus:ring-blue-600 focus:border-blue-600 block w-full p-2.5" required>
                        </div>
                        <button type="submit" class="w-full text-white bg-blue-600 hover:bg-blue-700 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Login</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</body>
<script src="https://unpkg.com/flowbite@1.4.0/dist/flowbite.js"></script>
</html>
