@extends('admin.templateadmin')
@section('content')



<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-100 overflow-auto">
  <h4 class="text-2xl font-bold text-black mb-4 mt-2">Edit Produk</h4>

  <form method="POST" action="/admin/produk/{{$post[0]->produk_id}}" enctype="multipart/form-data">
    @method('PUT')
    @csrf
    <div class="grid md:grid-cols-3 gap-2">
      <div class="mb-2 col-span-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Nama Produk</label>
        <input type="text" name="nama_produk" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Videotron P. 1,5 Inch" required autofocus value="{{$post[0]->nama_produk}}">
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Ukuran</label>
        <input type="text" name="ukuran" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="1,5 Inch" required value="{{$post[0]->ukuran}}">
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Sistem Operasi</label>
        <select name="sistem_operasi" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required>
          <option value="{{$post[0]->sistem_operasi}}">{{$post[0]->sistem_operasi}}</option>
          <option value="android">Android</option>
          <option value="windows">Windows</option>
          <option value="-">Tidak Ada</option>
        </select>
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Harga</label>
        <input type="text" id="harga" name="harga" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="15.000.000" required value="{{$post[0]->harga}}">
      </div>
      <div class="mb-3 row-span-4">
        <label for="" class="block mb-2 text-sm font-medium text-black">Spesifikasi</label>
        <textarea name="spesifikasi" rows="6" class="block p-2.5 w-full h-full text-sm text-black bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Jelaskan Spesifikasi Produk..." style="white-space: pre-wrap;" required>{{$post[0]->spesifikasi}}</textarea>
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Kategori</label>
        <select name="kategori_id" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required>
          <option value="{{$post[0]->kategori_id}}">{{$post[0]->nama_kategori}}</option>
          @forelse ($postKategori as $value)
          <option value="{{$value->id}}">{{$value->nama_kategori}}</option>
          @empty
          <option value=""></option>
          @endforelse
        </select>
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Lain - Lain</label>
        <select name="lain_lain" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required>
          <option value="{{$post[0]->lain_lain}}">{{$post[0]->lain_lain}}</option>
          <option value="touchscreen">Touchscreen</option>
          <option value="nontouchscreen">Non Touchscreen</option>
          <option value="-">Tidak Ada</option>
        </select>
      </div>
      <div class="mb-3">
        <label for="" class="block mb-2 text-sm font-medium text-black">Upload Foto</label>
        <input name="upload_foto[]" class="block w-full p-1 text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer" id="file_input" type="file" multiple>
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Preview Photo</label>
        <div id="listPhoto">
        </div>
      </div>
      <div class="mb-3">
        <label for="" class="block mb-2 text-sm font-medium text-black">Upload Foto Detail</label>
        <input name="upload_detail[]" class="block w-full p-1 text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer" id="file_input" type="file" multiple>
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Preview Photo Detail</label>
        <div id="listPhotoDetail">
        </div>
      </div>
    </div>
    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center m-2 mb-4">Tambah</button>
    <a href="/admin/produk" class="text-black flex items-center p-2 text-bold w-24 hover:text-red-600">
      <svg class="w-4 h-auto" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
        <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
      </svg>
      Kembali
    </a>
  </form>
</div>

@endsection
@push('script')
<script type="text/javascript">
  $(document).ready(function(){
    $.ajax({
      type: "GET",
      url: "/admin/produk/{{$post[0]->produk_id}}",
      data : {},
      success: function(data){
        data = $.parseJSON(data)
        photoGallery = data.upload_foto.split(",");
        photoDetail = data.detail_foto.split(",");
        if (photoGallery.length == 0) {
          $('#listPhoto').html(`
          <ul class="space-y-4 text-left text-gray-500 dark:text-gray-400">
            <li class="flex items-center space-x-3">
                <span class="underline text-xs">empty photo</span>
            </li>
          </ul>
          `)
        }
        for (i=0; i<photoGallery.length; i++) {
          $("#listPhoto").append(`
          <ul class="space-y-4 text-left text-gray-500 dark:text-gray-400" >
            <li class="flex items-center space-x-3" id="`+photoGallery[i]+`">
                <a href="/upload/`+photoGallery[i]+`" id="detailFoto" class="cursor-pointer text-xs flex" data-modal-target="modalEl" data-modal-toggle="modalEl" class="underline text-xs" data-id="`+photoGallery[i]+`" target="_blank">`+photoGallery[i]+`
                </a>
                <a href="#" id="deletePhoto" data-id="`+photoGallery[i]+`" class="underline text-red-600"> 
                  <svg class="w-3 h-3 text-red-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                  </svg>
                </a>
                <input type="hidden" name="list_foto[]" value="`+photoGallery[i]+`">
            </li>
          </ul>
          `);
        }

        if (photoDetail.length == 0) {
          $('#listPhotoDetail').html(`
          <ul class="space-y-4 text-left text-gray-500 dark:text-gray-400">
            <li class="flex items-center space-x-3">
                <span class="underline text-xs">empty photo</span>
            </li>
          </ul>
          `)
        }
        for (i=0; i<photoDetail.length; i++) {
          $("#listPhotoDetail").append(`
          <ul class="space-y-4 text-left text-gray-500 dark:text-gray-400" >
            <li class="flex items-center space-x-3" id="`+photoDetail[i]+`">
                <a href="/upload/`+photoDetail[i]+`" id="detailFoto" class="cursor-pointer text-xs flex" data-modal-target="modalEl" data-modal-toggle="modalEl" class="underline text-xs" data-id="`+photoDetail[i]+`" target="_blank">`+photoDetail[i]+`
                </a>
                <a href="#" id="deletePhoto" data-id="`+photoDetail[i]+`" class="underline text-red-600"> 
                  <svg class="w-3 h-3 text-red-500" aria-hidden="true" xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 14 14">
                    <path stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="m1 1 6 6m0 0 6 6M7 7l6-6M7 7l-6 6"/>
                  </svg>
                </a>
                <input type="hidden" name="list_foto_detail[]" value="`+photoDetail[i]+`">
            </li>
          </ul>
          `);
        }

      }
    })
    $("#harga").keyup(function(e) {
      harga = $("#harga").val()
      regex = new RegExp(/[^\d,]+/g, '');
      if (harga.match(regex)) {
        Swal.fire({
          title: 'Inputan salah !',
          icon: 'error',
          html:
            'Hanya angka saja, tidak perlu huruf',
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText:
            '<i class="fa fa-thumbs-up"></i> OK!',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        })
        $(this).val('');
      }
      else {
        $(this).val(formatRupiah(harga));
      }
    });
  })

  function formatRupiah(num) {
      var str = num.toString().replace("", ""),
          parts = false,
          output = [],
          i = 1,
          formatted = null;
      if (str.indexOf(".") > 0) {
          parts = str.split(".");
          str = parts[0];
      }
      str = str.split("").reverse();
      for (var j = 0, len = str.length; j < len; j++) {
          if (str[j] != ",") {
              output.push(str[j]);
              if (i % 3 == 0 && j < (len - 1)) {
                  output.push(",");
              }
              i++;
          }
      }
      formatted = output.reverse().join("");
      return ("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
  };

  $(document).on('click', '#deletePhoto', function(e){
    e.preventDefault();
    id = $(this).data("id")
    Swal.fire({
      title: 'Yakin hapus foto ?',
      icon: 'warning',
      html:
        'Foto yang di hapus tidak dapat dipulihkan kembali',
      showCloseButton: true,
      showCancelButton: true,
      focusConfirm: false,
      confirmButtonText:
        '<i class="fa fa-thumbs-up"></i> OK!',
      confirmButtonAriaLabel: 'Thumbs up, great!',
      cancelButtonText:
        '<i class="fa fa-thumbs-down"></i> Batal',
      cancelButtonAriaLabel: 'Thumbs down'
    }).then(function(isConfirmed){
      if (isConfirmed){
        $.ajax({
          type : "GET",
          url : "/admin/produk/photo/{{$post[0]->produk_id}}/"+id,
          data: {},
          success: function(data){
            document.getElementById(id).remove();
          }
        })
      }
    })
  })
</script>
@endpush