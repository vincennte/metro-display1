@extends('admin.templateadmin')
@section('content')



<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-100 overflow-auto">
    <h4 class="text-2xl font-bold text-black mb-4 mt-2">Edit Kategori Produk</h4>
    
<form method="POST" action="/admin/kategori/{{$post->id}}">
    @csrf
    @method('PUT')
    <div class="mb-6">
      <label for="" class="block mb-2 text-sm font-medium text-black">Nama Kategori Produk</label>
      <input type="text" name="nama_kategori" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Videotron P. 1,5 Inch" value="{{$post->nama_kategori}}" required>
    </div>
    <div class="mb-6">   
        <label for="" class="block mb-2 text-sm font-medium text-black">Deskripsi</label>
        <textarea name="deskripsi" rows="6" class="block p-2.5 w-full text-sm text-black bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Deskripsi Kategori Produk..."required>{{$post->deskripsi}}</textarea>
    </div>
    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center">Edit</button>
    <a href="/admin/kategori" class="text-black flex items-center p-2 text-bold w-24 hover:text-red-600">
        <svg class="w-4 h-auto" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
          </svg>
        Kembali
    </a>
    </form>
  
</div>

@endsection