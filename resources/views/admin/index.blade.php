@extends('admin.templateadmin')
@section('content')


<div class="p-2 w-full flex justify-start md:gap-4 md:grid-cols-4 gap-2 grid grid-cols-2">
    <div class="overflow-auto w-full p-2 bg-white border border-gray-200 rounded-lg shadow border border-gray hover:bg-gradient-to-r hover:from-cyan-300 hover:to-blue-300 hover:scale-[1.03] transition-all duration-500 cursor-pointer">
        <p class="text-6xl font-bold hover:text-white bg-clip-text text-transparent bg-clip-text bg-gradient-to-r from-slate-500 to-slate-900">{{$produk->count()}}</p>
        <h5 class="text-xl font-semibold tracking-tight text-gray-700 hover:text-white">Total Produk</h5>
    </div>
    <div class="overflow-auto w-full p-2 bg-white border border-gray-200 rounded-lg shadow border border-gray hover:bg-gradient-to-r hover:from-cyan-300 hover:to-blue-300 hover:scale-[1.03] transition-all duration-500 cursor-pointer">
        <p class="text-6xl font-bold hover:text-white bg-clip-text text-transparent bg-clip-text bg-gradient-to-r from-slate-500 to-slate-900">{{$kategori->count()}}</p>
        <h5 class="text-xl font-semibold tracking-tight text-gray-700 hover:text-white">Total Kategori</h5>
    </div>
    <div class="overflow-auto w-full p-2 bg-white rounded-lg shadow hover:bg-gradient-to-r hover:from-indigo-300 hover:to-blue-300 hover:scale-[1.03] transition-all duration-500 cursor-pointer">
        <p class="text-6xl font-bold text-gray-700 hover:text-white bg-clip-text text-transparent bg-clip-text bg-gradient-to-r from-slate-500 to-slate-900">{{$pesan->count()}}</p>
        <h5 class="text-xl font-semibold tracking-tight text-gray-700 hover:text-white">Pesan Masuk</h5>
    </div>
    <div class="overflow-auto w-full p-2 bg-white rounded-lg shadow hover:bg-gradient-to-r hover:from-indigo-300 hover:to-blue-300 hover:scale-[1.03] transition-all duration-500 cursor-pointer">
        <p class="text-6xl font-bold text-gray-700 hover:text-white bg-clip-text text-transparent bg-clip-text bg-gradient-to-r from-slate-500 to-slate-900">{{$visitors[0]->result}}</p>
        <h5 class="text-xl font-semibold tracking-tight text-gray-700 hover:text-white">Total Kunjungan</h5>
    </div>
</div>
<div class="relative overflow-x-auto p-2 sm:rounded-lg md:flex h-auto">
    <div class="w-full overflow-auto p-2 border border-gray-200 bg-white shadow hover:scale-[1.01] transition-all duration-500">
        <h4 class="text-2xl font-bold text-black mb-4 mt-2 text-center">Grafik kunjungan</h4>
        <div class="w-auto h-32 flex items-center justify-center">
            <canvas id="chartVisitors"></canvas>
        </div>
    </div>
</div>
<div class="relative overflow-x-auto p-2 rounded-lg md:grid md:grid-cols-3 gap-2 h-96">
    <div class="w-full overflow-auto p-2 col-span-2 border border-gray-200 bg-white shadow hover:scale-[1.01] transition-all duration-500">
        <h4 class="text-2xl font-bold text-black mb-4 mt-2 text-center">List Produk</h4>
        <table class="text-sm text-gray-500 border border-gray-300 object-contain m-auto">
            <thead class="text-xs text-center text-gray-700 uppercase bg-gray-100 border border-gray-300 shadow">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        No
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Nama Produk
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Ukuran
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Harga
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Link Produk
                    </th>
                    
                </tr>
            </thead>
            <tbody>
                <input type="hidden" id="count" value="{{count($produk)}}">
                @forelse ($produk as $no => $val)
                <tr class="bg-white border-b hover:bg-gray-50">
                    <td class="px-6 py-4 text-center">
                        {{$no+1}}
                    </td>
                    <th scope="row" class="px-6 py-4 font-medium text-black whitespace-nowrap">
                        {{$val->nama_produk}}
                    </th>
                    <td class="px-6 py-4">
                        {{$val->ukuran}}
                    </td>
                    <td class="px-6 py-4 whitespace-nowrap">
                        Rp. <a id="harga{{$no}}">{{$val->harga}} </a>
                    </td>
                    <td class="px-6 py-4">
                        <a href="/universal/produk/{{$val->id}}" target="blank">
                            <svg fill="none" class="w-5 h-5 m-auto" stroke="blue" stroke-width="2" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25"></path>
                              </svg>
                        </a>
                    </td>
                </tr>
                @empty
                <tr class="bg-white border-b hover:bg-gray-50">
                    <td colspan="4">
                        Data Kosong
                    </td>
                </tr>
                @endforelse
                
            </tbody>
        </table>
    </div>
    <div class="w-full overflow-y-auto p-2 border border-gray-200 bg-white shadow hover:scale-[1.01] transition-all duration-500">
        <div class="w-full h-full overflow-auto p-2 border border-gray-200 bg-white shadow hover:scale-[1.01] transition-all duration-500">
            <h4 class="text-2xl font-bold text-black mb-4 mt-2 text-center">Performa Kunjungan Website Marketing</h4>
            <div class="w-auto h-full flex items-center justify-center">
                <canvas id="chartMarketing"></canvas>
            </div>
        </div>
    </div>
</div>
<div class="relative overflow-x-auto p-2 sm:rounded-lg md:flex h-96">
    <div class="w-full overflow-auto p-2 border border-gray-200 bg-white shadow hover:scale-[1.01] transition-all duration-500">
        <h4 class="text-2xl font-bold text-black mb-4 mt-2 text-center">Pesan Masuk</h4>
        <table class="text-sm text-gray-500 border border-gray-300 m-auto">
            <thead class="text-xs text-center text-gray-700 uppercase bg-gray-100 border border-gray-300 shadow">
                <tr>
                    <th scope="col" class="px-6 py-3">
                        Nama Lengkap
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Email
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Phone
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Nama Marketing
                    </th>
                    <th scope="col" class="px-6 py-3">
                        Pesan
                    </th>
                </tr>
            </thead>
            <tbody>
                @forelse ($pesan as $val)
                <tr class="bg-white border-b hover:bg-gray-50">
                    <th scope="row" class="px-6 py-4 font-medium text-black whitespace-nowrap ">
                        {{$val->nama_customer}}
                    </th>
                    <td class="px-6 py-4">
                        {{$val->email_customer}}
                    </td>
                    <td class="px-6 py-4">
                        {{$val->no_hp}}
                    </td>
                    <td class="px-6 py-4">
                        {{$val->nama_marketing}}
                    </td>
                    <td class="px-6 py-4">
                        {{$val->pesan}}
                    </td>
                </tr>
                @empty
                    
                @endforelse
                
            </tbody>
        </table>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
    $(document).ready(function(){
        function formatRupiah(num) {
            var str = num.toString().replace("", ""),
                parts = false,
                output = [],
                i = 1,
                formatted = null;
            if (str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for (var j = 0, len = str.length; j < len; j++) {
                if (str[j] != ",") {
                    output.push(str[j]);
                    if (i % 3 == 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");
            return ("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
        };
        countData = parseInt($("#count").val());
        for (i = 0; i < countData; i++) {
            valueHarga =  formatRupiah(parseInt($("#harga"+i).text()))
            $("#harga"+i).text(valueHarga);
        }
    })
</script>
<script>
    const chartVisitors = document.getElementById('chartVisitors');

    $.ajax({
            type : "GET",
            url : "/admin/getvisitors",
            data : {},
            success: function(data){
                new Chart(chartVisitors, {
                    type: 'line',
                    data: {
                        labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni','Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                        datasets: [{
                        data: data,
                        borderWidth: 1
                        }]
                    },
                    options: {
                        animations: {
                            tension: {
                                duration: 1000,
                                easing: 'linear',
                                from: 1,
                                to: 0,
                                loop: true
                            }
                        },
                        responsive : true,
                        plugins: {
                            legend: {
                            display: false
                            }
                        },
                        maintainAspectRatio: false,
                        aspectRatio: 2,
                        scales: {
                            x: {
                                type: 'category',
                                labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
                                beginAtZero: true,
                            },
                            y: {
                                beginAtZero: true,
                                max: 'auto'
                            }
                        },
                    }
                });
            }
        })
        const chartMarketing = document.getElementById('chartMarketing');
    $.ajax({
        type : "GET",
        url : "/admin/getallmarketing",
        data : {},
        success: function(data){
            marketingNames = []
            marketingVisitors = []

            for (i = 0; i<data.length; i++){
                marketingNames.push(data[i].nama_lengkap)
                marketingVisitors.push(data[i].pengunjung)
            }
            new Chart(chartMarketing, {
                type: 'bar',
                data: {
                    labels: marketingNames,
                    datasets: [{
                    data: marketingVisitors,
                    borderWidth: 1
                    }]
                },
                options: {
                    indexAxis: 'y',
                    responsive : true,
                    plugins: {
                        legend: {
                        display: false
                        }
                    },
                    maintainAspectRatio: false,
                    aspectRatio: 2,
                    scaleShowValues: true,
                    scales: {
                        y: {
                            ticks: {
                                autoSkip: false
                            }
                        }
                    }
                }
            });
        }
    })
                
  </script>
@endpush