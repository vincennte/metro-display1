@extends('admin.templateadmin')
@section('content')



<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-96 overflow-auto">
    <h4 class="text-2xl font-bold text-black mb-4 mt-2">User Marketing</h4>
    <div class="flex items-center justify-between pb-4">
        <div>
            <a href='/admin/marketing/create'>
                <button type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 md:font-medium font-xs rounded-lg text-sm px-5 py-2.5 mr-2 mb-2">+Tambah User</button>
            </a>
        </div>
    </div>
    <table class="text-sm text-gray-500 border border-gray-300 table-fixed">
        <thead class="text-xs text-center text-gray-700 uppercase bg-gray-100 border border-gray-300 shadow">
            <tr>
                <th scope="col" class="px-6 py-3 ">
                    No
                </th>
                <th scope="col" class="px-6 py-3">
                    Nama Marketing
                </th>
                <th scope="col" class="px-6 py-3">
                    Username
                </th>
                <th scope="col" class="px-6 py-3">
                    No. Handphone
                </th>
                <th scope="col" class="px-6 py-3">
                    Link Tokopedia
                </th>
                <th scope="col" class="px-6 py-3">
                    Generate Link
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($post as $no => $value)
            <tr class="bg-white border-b hover:bg-gray-50 ">
                <td class="w-4 p-4 text-center">
                    {{$no+1}}
                </td>
                <th scope="row" class="px-6 py-4 font-medium text-black">
                    {{$value->nama_lengkap}}
                </th>
                <td class="px-6 py-4">
                    {{$value->username}}
                </td>
                <td class="w-4 p-4 text-center">
                    {{$value->no_hp}}
                </td>
                <td class="w-4 p-4 text-center">
                    {{$value->link_tokped}}
                </td>
                <td class="w-4 p-4 text-center">
                    https://metrodisplayadvpro.com/{{$value->username}}
                </td>
                <td class="px-6 py-4 text-center flex gap-2">
                    <a href='/admin/marketing/{{$value->id}}/edit' class="text-white bg-yellow-600 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm w-24 px-5 py-2.5">Edit</a>
                    <form method="POST" action="/admin/marketing/{{$value->id}}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm w-24 px-5 py-2.5">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>  
@endsection