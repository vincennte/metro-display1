@extends('admin.templateadmin')
@section('content')



<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-100 overflow-auto">
  <h4 class="text-2xl font-bold text-black mb-4 mt-2">Tambah Banner</h4>
  <form method="POST" action="/admin/banner" enctype="multipart/form-data">
    @csrf 
    <div class="grid md:grid-cols-2 gap-3">
      <div class="mb-2 col-span-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Nama Banner</label>
        <input type="text" name="nama_banner" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Videotron P. 1,5 Inch" required autofocus>
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Upload Banner</label>
        <input name="upload_banner" class="block w-full p-1 text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50" id="upload_banner" type="file" accept="image/*">
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Link</label>
        <input type="text" id="link" name="link" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="link kategori/link lain" required>
      </div>
      <div class="mb-2">
        <label for="" class="block mb-2 text-sm font-medium text-black">Status Aktif</label>
        <select name="status"class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"required>
          <option value="1">Aktif</option>
          <option value="0">Tidak Aktif</option>
        </select>
      </div>
    </div>
      <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center m-2 mb-4">Tambah</button>
      <a href="/admin/banner" class="text-black flex items-center p-2 text-bold w-24 hover:text-red-600">
          <svg class="w-4 h-auto" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
              <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
            </svg>
          Kembali
        </a>
    </form>
</div>
@endsection