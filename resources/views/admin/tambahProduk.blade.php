@extends('admin.templateadmin')
@section('content')



<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-100 overflow-auto">
    <h4 class="text-2xl font-bold text-black mb-4 mt-2">Tambah Produk</h4>
    
<form method="POST" action="/admin/produk" enctype="multipart/form-data">
  @csrf
  <div class="grid md:grid-cols-3 gap-2">
    <div class="mb-2 col-span-2">
      <label for="" class="block mb-2 text-sm font-medium text-black">Nama Produk</label>
      <input type="text" name="nama_produk" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Videotron P. 1,5 Inch" required autofocus>
    </div>
    <div class="mb-2">
      <label for="" class="block mb-2 text-sm font-medium text-black">Ukuran</label>
      <input type="text" name="ukuran" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="1,5 Inch" required>
    </div>
    <div class="mb-2">
      <label for="" class="block mb-2 text-sm font-medium text-black">Sistem Operasi</label>
      <select name="sistem_operasi"class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"required>
        <option value="android">Android</option>
        <option value="windows">Windows</option>
        <option value="-">Tidak Ada</option>
      </select>
    </div>
    <div class="mb-2">
      <label for="" class="block mb-2 text-sm font-medium text-black">Harga</label>
      <input type="text" id="harga" name="harga" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="15.000.000" required>
    </div>
    <div class="mb-3 row-span-4">   
      <label for="" class="block mb-2 text-sm font-medium text-black">Spesifikasi</label>
      <textarea name="spesifikasi" rows="6" class="block p-2.5 w-full h-full text-sm text-black bg-gray-50 rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500" placeholder="Jelaskan Spesifikasi Produk..." style="white-space: pre-wrap;"></textarea>
  </div>
    <div class="mb-2">
      <label for="" class="block mb-2 text-sm font-medium text-black">Kategori</label>
      <select name="kategori_id"class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"required>
        <option value=""></option>
        @forelse ($post as $value)
          <option value="{{$value->id}}">{{$value->nama_kategori}}</option>
        @empty
        <option value=""></option>
        @endforelse
      </select>
    </div>
    <div class="mb-3">   
      <label for="" class="block mb-2 text-sm font-medium text-black">Upload Foto</label>
      <input name="upload_foto[]" class="block w-full p-1 text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50" id="file_input" type="file" multiple>
      <p class="text-xs italic b">*untuk slide display produk</p>
    </div>
    <div class="mb-2">
      <label for="" class="block mb-2 text-sm font-medium text-black">Lain - Lain</label>
      <select name="lain_lain"class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"required>
        <option value="touchscreen">Touchscreen</option>
        <option value="nontouchscreen">Non Touchscreen</option>
        <option value="-">Tidak Ada</option>
      </select>
    </div> 
    <div class="mb-3">   
      <label for="" class="block mb-2 text-sm font-medium text-black">Upload Detail Produk</label>
      <input name="detail_foto[]" class="block w-full p-1 text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50" id="file_input" type="file" multiple>
      <p class="text-xs italic b">*untuk detail spesifikasi dalam bentuk gambar </p>
    </div>
  </div>
    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center m-2 mb-4">Tambah</button>
    <a href="/admin/produk" class="text-black flex items-center p-2 text-bold w-24 hover:text-red-600">
        <svg class="w-4 h-auto" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
          </svg>
        Kembali
    </a>
  </form>
</div>

@endsection
@push('script')
<script type="text/javascript">
  $(document).ready(function(){
    $("#harga").keyup(function(e) {
      harga = $("#harga").val()
      regex = new RegExp(/[^\d,]+/g, '');
      if (harga.match(regex)) {
        Swal.fire({
          title: 'Inputan salah !',
          icon: 'error',
          html:
            'Hanya angka saja, tidak perlu huruf',
          showCloseButton: true,
          showCancelButton: true,
          focusConfirm: false,
          confirmButtonText:
            '<i class="fa fa-thumbs-up"></i> OK!',
          confirmButtonAriaLabel: 'Thumbs up, great!',
        })
        $(this).val('');
      }
      else {
        $(this).val(formatRupiah(harga));
      }
    });

    function formatRupiah(num) {
        var str = num.toString().replace("", ""),
            parts = false,
            output = [],
            i = 1,
            formatted = null;
        if (str.indexOf(".") > 0) {
            parts = str.split(".");
            str = parts[0];
        }
        str = str.split("").reverse();
        for (var j = 0, len = str.length; j < len; j++) {
            if (str[j] != ",") {
                output.push(str[j]);
                if (i % 3 == 0 && j < (len - 1)) {
                    output.push(",");
                }
                i++;
            }
        }
        formatted = output.reverse().join("");
        return ("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
    };
  })
</script>
@endpush