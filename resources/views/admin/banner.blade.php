@extends('admin.templateadmin')
@section('content')

<section class="">
    <div class="container">
        <div class="px-5 py-5 rounded">
        <div class="flex flex-wrap">
            <div class="w-full mb-0 md:mb-0">
                <a class="flex items-center text-black font-medium">
                    <span>Page ini berfungsi untuk mengatur gambar mana saja yang akan muncul pada landing page </span>
                </a>
            </div>
        </div>
        </div>
    </div>
</section>
<div>
    <a href='/admin/banner/create'>
        <button type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 md:font-medium font-xs rounded-lg text-sm px-5 py-2.5 mr-2 mb-2">+Tambah Banner</button>
    </a>
</div>
<div class="relative overflow-x-auto shadow-md sm:rounded-lg">
    <table class="w-full text-sm text-left text-gray-500">
        <thead class="text-xs text-gray-700 uppercase bg-gray-50">
            <tr>
                <th scope="col" class="px-6 py-3">
                    No
                </th>
                <th scope="col" class="px-6 py-3">
                    Nama Produk
                </th>
                <th scope="col" class="px-6 py-3">
                    Gambar
                </th>
                <th scope="col" class="px-6 py-3">
                    Link
                </th>
                <th scope="col" class="px-6 py-3">
                    Status
                </th>
                <th scope="col" class="px-6 py-3">
                    Aksi
                </th>
            </tr>
        </thead>
        <tbody>
            @forelse ($post as $no => $value)
            <tr class="bg-white border-b hover:bg-gray-50">
                <th class="px-6 py-4">
                    {{$no+1}}
                </th>
                <td class="px-6 py-4">
                    {{$value->nama_banner}}
                </td>
                <td class="px-6 py-4">
                    <img src="{{asset('/upload/banner/'.$value->upload_banner)}}" width="50" height="50">
                </td>
                <td class="px-6 py-4">
                    {{$value->link}}
                </td>
                <td class="px-6 py-4">
                    <?php 
                        if($value->status == true){
                            echo "Aktif";
                        }else {
                            echo "Tidak Aktif";
                    }
                    ?>
                </td>
                <td class="px-6 py-4 text-right">
                    <a id="editData" class="cursor-pointer text-xm flex text-blue-500" data-modal-target="modalEl" data-modal-toggle="modalEl" data-id="{{$value->id}}">
                        <u>Edit</u>
                    </a>
                </td>
            </tr>
            @empty
            <tr class="bg-white border-b hover:bg-gray-50">
                Banner Kosong
            </tr>
            @endforelse
        </tbody>
    </table>
</div>

<div id="modalEl" tabindex="-1" aria-hidden="true" data-modal-backdrop="false" class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] md:h-full">
    <div class="relative w-full h-full max-w-2xl md:h-auto">
        <!-- Modal content -->
        <div class="relative bg-white rounded-lg shadow">
            <!-- Modal header -->
            <div class="flex items-start justify-between p-4 border-b rounded-t">
                <h3 class="text-xl font-semibold text-black">
                    Edit Banner
                </h3>
                <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="modalEl">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="p-4">
                <form method="POST" action="" enctype="multipart/form-data" id="editForm">
                    @method('PUT')
                    @csrf 
                    <div class="grid md:grid-cols-2 gap-3">
                    <div class="mb-2 col-span-2">
                        <label for="" class="block mb-2 text-sm font-medium text-black">Nama Banner</label>
                        <input type="text" name="nama_banner" id="nama_banner" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Videotron P. 1,5 Inch" required autofocus>
                    </div>
                    <div class="mb-2">
                        <label for="" class="block mb-2 text-sm font-medium text-black">Upload Banner</label>
                        <input name="upload_banner" id="upload_banner" class="block w-full p-1 text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50" id="upload_banner" type="file" accept="image/*">
                    </div>
                    <div class="mb-2">
                        <label for="" class="block mb-2 text-sm font-medium text-black">Link</label>
                        <input type="text" id="link" name="link" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="link kategori/link lain" required>
                    </div>
                    <div class="mb-2">
                        <label for="" class="block mb-2 text-sm font-medium text-black">Status Aktif</label>
                        <select name="status" id="status" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"required>
                        <option value="" id="optVal"> </option>
                        <option value="1">Aktif</option>
                        <option value="0">Tidak Aktif</option>
                        </select>
                    </div>
                    </div>
            </div>
            <!-- Modal footer -->
            <div class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b">
                <button data-modal-hide="modalEl" type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Simpan dan Tutup</button>
            </div>
                </form>
        </div>
    </div>
</div>
@endsection
@push('script')
<script type="text/javascript">
    $(document).on('click','#editData',function () {
        url = "/admin/banner/" + $(this).data('id');
        console.log(url);
        $.ajax({
            type : "GET",
            url : url,
            data : {
                id : $(this).data('id')
            },
            success: function(data){
                data = $.parseJSON(data);  

                status = data['status'];
                if (status == 1){
                    status = "Aktif"
                }else {
                    status = "Tidak Aktif"
                }

                $('#nama_banner').val(data['nama_banner']);
                $('#link').val(data['link']);
                $('#optVal').val(data['status']);
                $('#optVal').text(status);
                $('#editForm').attr("action",url);
            }

        })
    });
</script>
@endpush