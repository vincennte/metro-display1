@extends('admin.templateadmin')
@section('content')



<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-100 overflow-auto">
    <h4 class="text-2xl font-bold text-black mb-4 mt-2">Edit Users</h4>
    
<form method="POST" action="/admin/users/{{$post->id}}">
  @method('PUT')
  @csrf
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Nama User</label>
      <input type="text" name="name" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Vincencius" value="{{$post->name}}" required >
    </div>
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Email</label>
      <input type="email" name="email" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="email@mail.com" value="{{$post->email}}" required>
    </div>
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Role</label>
      <select name="role" class="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required>
        <option value="{{$post->role}}">{{$post->role}}</option>
        <option value="superadmin">Super Admin</option>
        <option value="admin">Admin</option>
        <option value="user">Users</option>
      </select>
    </div>
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Password</label>
      <input type="password" name="password" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" required>
    </div>
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Phone Number</label>
      <input type="number" name="phone_number" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" value="{{$post->phone_number}}"required>
    </div>
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Tokopedia Link</label>
      <input type="text" name="link_tokped" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" value="{{$post->link_tokped}}" required>
    </div>
    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center m-2 mb-2">Tambah</button>
    <a href="/admin/users" class="text-black flex items-center p-2 text-bold w-24 hover:text-red-600">
        <svg class="w-4 h-auto" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
          </svg>
        Kembali
    </a>
  </form>
  
</div>

@endsection