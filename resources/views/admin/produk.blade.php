@extends('admin.templateadmin')
@section('content')
<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-96 overflow-auto">
    <h4 class="text-2xl font-bold text-black mb-4 mt-2">Produk</h4>
    <div class="flex items-center justify-between pb-4">
        <div>
            <a href='/admin/produk/create'>
                <button type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 md:font-medium font-xs rounded-lg text-sm px-5 py-2.5 mr-2 mb-2">+Tambah Produk</button>
            </a>
        </div>
        <label for="table-search" class="sr-only">Search</label>
        <div class="relative">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg class="w-5 h-5 text-gray-500" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
            </div>
            <input type="text" id="table-search" class="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-300 focus:border-blue-500" placeholder="Cari Produk">
        </div>
    </div>
    <table class="text-sm text-gray-500 border border-gray-300 table-fixed">
        <thead class="text-xs text-center text-gray-700 uppercase bg-gray-100 border border-gray-300 shadow">
            <tr>
                <th scope="col" class="px-6 py-3 ">
                    No
                </th>
                <th scope="col" class="px-6 py-3">
                    Nama Produk
                </th>
                <th scope="col" class="px-6 py-3">
                    Ukuran
                </th>
                <th scope="col" class="px-6 py-3">
                    Sistem Operasi
                </th>
                <th scope="col" class="px-6 py-3 whitespace-nowrap">
                    Lain - Lain
                </th>
                <th scope="col" class="px-6 py-3">
                    Kategori
                </th>
                <th scope="col" class="px-6 py-3">
                    Harga
                </th>
                <th scope="col" class="px-6 py-3">
                    Detail
                </th>
                <th scope="col" colspan="2" class="px-6 py-3">
                    Opsi
                </th>
            </tr>
        </thead>
        <tbody>
            <input type="hidden" id="count" value="{{count($post)}}">
            @forelse ($post as $no => $value)
                <tr class="bg-white border-b hover:bg-gray-50 ">
                    <td class="w-4 p-4 text-center">
                        {{$no+1}}
                    </td>
                    <th scope="row" class="px-6 py-4 font-medium text-black">
                        {{$value->nama_produk}}
                    </th>
                    <td class="px-6 py-4">
                        {{$value->ukuran}}
                    </td>
                    <td class="w-4 p-4 text-center">
                        {{$value->sistem_operasi}}
                    </td>
                    <th scope="row" class="px-6 py-4 font-medium text-black">
                        {{$value->lain_lain}}
                    </th>
                    <td class="px-6 py-4">
                        {{$value->nama_kategori}}
                    </td>
                    <td class="w-4 p-4 text-center">
                        Rp. <a id="harga{{$no}}">{{$value->harga}} </a>
                    </td>
                    <td class="px-6 py-4 text-center">
                        <a id="detailData" class="cursor-pointer text-xm flex" data-modal-target="modalEl" data-modal-toggle="modalEl" data-id="{{$value->produk_id}}">
                            <svg class=' m-auto w-4 h-auto text-bold' stroke="blue" stroke-width="2.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                                <path stroke-linecap="round" stroke-linejoin="round" d="M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25"></path>
                            </svg>
                        </a>
                    </td>
                    <td class="px-6 py-4 text-center">
                        <a href='/admin/produk/{{$value->produk_id}}/edit' class="items-center text-white bg-yellow-600 hover:bg-yellow-500 focus:ring-4 focus:ring-yellow-300 font-medium rounded-lg text-sm px-5 py-2.5 w-20">Edit</a>
                    </td>
                    <td class="px-6 py-4 text-center ">
                        <form method="POST" action="/admin/produk/{{$value->produk_id}}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="items-center text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm px-5 py-2.5 w-20" >Hapus</button>
                        </form>
                    </td>
                    @empty
                    <tr class="bg-white border-b hover:bg-gray-50">
                        <td class="w-4 p-4 text-center" colspan="10">
                            Data Kosong
                        </td>
                    </tr>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>  
   
<!-- Main modal -->
<div id="modalEl" tabindex="-1" aria-hidden="true" data-modal-backdrop="false" class="fixed top-0 left-0 right-0 z-50 hidden w-full p-4 overflow-x-hidden overflow-y-auto md:inset-0 h-[calc(100%-1rem)] md:h-full">
    <div class="relative w-full h-full max-w-2xl md:h-auto">
        <!-- Modal content -->
        <div class="relative bg-white rounded-lg shadow">
            <!-- Modal header -->
            <div class="flex items-start justify-between p-4 border-b rounded-t">
                <h3 class="text-xl font-semibold text-black">
                    Preview Produk
                </h3>
                <button type="button" class="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1.5 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white" data-modal-hide="modalEl">
                    <svg aria-hidden="true" class="w-5 h-5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    <span class="sr-only">Close modal</span>
                </button>
            </div>
            <!-- Modal body -->
            <div class="p-2 space-y-2 overflow-y-scroll h-96">
                <div class="grid grid-cols-1 md:grid-cols-2 p-4">
                    <div class="h-56">
                        <div id="default-carousel" class="relative w-full" data-carousel="slide">
                            <div class="grid grid-cols-2 md:grid-cols-2 gap-4" id="carouselModal">
                            </div>
                        </div>
                    </div>
                    <div class="h-80">
                        <h1 id="titleModal" class="w-full text-4xl font-bold text-dark mb-2">
                            
                        </h1>
                        <p class=" text-base leading-relaxed text-body-color font-bold italic">
                            Ukuran : </br>
                            <button id="ukuranModal" type="button" class="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mx-2 mb-2"></button>
                        </p>
                        <p class=" text-base leading-relaxed text-body-color font-bold italic">
                            Sistem Operasi :</br>
                            <button id="sistemOperasiModal" type="button" class="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mx-2 mb-2"></button>
                        </p>
                        <p class=" text-base leading-relaxed text-body-color font-bold italic">
                            Lain - Lain : </br>
                            <button type="button" id="lainLainModal" class="text-gray-900 bg-white border border-gray-300 focus:outline-none hover:bg-gray-100 focus:ring-4 focus:ring-gray-200 font-medium rounded-lg text-sm px-5 py-2.5 mx-2 mb-2"></button>
                        </p>
                        <p class=" text-base leading-relaxed text-body-color font-bold italic">
                            Spesifikasi :
                        </p>
                        <div id="spesifikasiModal" class="text-base leading-relaxed text-body-color h-40 overflow-auto mb-2">
                            <textarea class="text-base leading-relaxed text-body-color h-40 overflow-auto mb-2 w-full" disabled></textarea>
                        </div>
                    </div>
                </div>
                <div id="detailPhoto" class=""> </div>
            </div>
            <!-- Modal footer -->
            <div class="flex items-center p-6 space-x-2 border-t border-gray-200 rounded-b">
                <button data-modal-hide="modalEl" type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Tutup</button>
            </div>
        </div>
    </div>
</div>
  
 
@endsection
@push('script')
<script type="text/javascript">
    $(document).ready(function(){
        function formatRupiah(num) {
            var str = num.toString().replace("", ""),
                parts = false,
                output = [],
                i = 1,
                formatted = null;
            if (str.indexOf(".") > 0) {
                parts = str.split(".");
                str = parts[0];
            }
            str = str.split("").reverse();
            for (var j = 0, len = str.length; j < len; j++) {
                if (str[j] != ",") {
                    output.push(str[j]);
                    if (i % 3 == 0 && j < (len - 1)) {
                        output.push(",");
                    }
                    i++;
                }
            }
            formatted = output.reverse().join("");
            return ("" + formatted + ((parts) ? "." + parts[1].substr(0, 2) : ""));
        };
        countData = parseInt($("#count").val());
        for (i = 0; i < countData; i++) {
            valueHarga =  formatRupiah(parseInt($("#harga"+i).text()))
            $("#harga"+i).text(valueHarga);
        }
    })
    $(document).on('click','#detailData',function () {
        produkId = $(this).data('id');
        $.ajax({
            type : "GET",
            url : "/admin/produk/" + $(this).data('id'),
            data : {
                id : $(this).data('id')
            },
            success: function(data){
                data = $.parseJSON(data);
                image = data['upload_foto'].split(',')
                detailFoto = data['detail_foto'].split(',')
                detailPhoto = $('#detailPhoto')
                listGambar= $('#carouselModal')
                listGambar.html(``)
                $('#titleModal').text(data['nama_produk']);
                $('#ukuranModal').text(data['ukuran']);
                $('#sistemOperasiModal').text(data['sistem_operasi']);
                $('#lainLainModal').text(data['lain_lain']);
                $('#spesifikasiModal').text(data['spesifikasi']);
                $.each(image, function(i, val){
                    listGambar.append(
                    `<div>
                        <img class="h-auto max-w-full rounded-lg" src="{{asset('/upload/`+val+`')}}" alt="">
                    </div>
                    `)
                })
                $.each(detailFoto, function(i, val){
                    detailPhoto.append(
                    `<div>
                        <img class="rounded-lg" width="auto" height="auto"  src="{{asset('/upload/detail/`+val+`')}}" alt="">
                    </div>
                    `)
                })
            }

        })
    });
</script>
@endpush