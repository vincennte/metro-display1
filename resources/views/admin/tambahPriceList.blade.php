@extends('admin.templateadmin')
@section('content')



<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-100 overflow-auto">
    <h4 class="text-2xl font-bold text-black mb-4 mt-2">Upload Price List</h4>
    
<form method="POST" action="/admin/pricelist" enctype="multipart/form-data">
  @csrf
    <div class="mb-3">   
      <label for="" class="block mb-2 text-sm font-medium text-black">Upload Price List</label>
      <input name="file" class="block w-full p-1 text-sm text-gray-900 border border-gray-300 rounded-lg cursor-pointer bg-gray-50" id="file" type="file" accept=".pdf">
      <p class="text-xs italic b">*untuk user bisa download katalog berisi harga</p>
    </div>
    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center m-2 mb-4">Tambah</button>
    <a href="/admin/pricelist" class="text-black flex items-center p-2 text-bold w-24 hover:text-red-600">
        <svg class="w-4 h-auto" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
          </svg>
        Kembali
    </a>
  </form>
</div>

@endsection
@push('script')

@endpush