@extends('admin.templateadmin')
@section('content')



<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-100 overflow-auto">
    <h4 class="text-2xl font-bold text-black mb-4 mt-2">Tambah Marketing</h4>
    
  <form method="POST" action="/admin/marketing/{{$post->id}}">
    @method('PUT')
    @csrf
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Nama Marketing</label>
      <input type="text" name="nama_lengkap" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="Avril Lavigne" value="{{$post->nama_lengkap}}" required>
    </div>
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Username</label>
      <input type="text" name="username" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" placeholder="avril96" value="{{$post->username}}" disabled>
      <p class="text-xs italic b">*username tidak boleh diganti</p>
    </div>
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Phone Number</label>
      <input type="number" name="no_hp" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" value="{{$post->no_hp}}" placeholder="62873259325" required>
      <p class="text-xs italic b">*diawali dengan 628...</p>
    </div>
    <div class="mb-2 md:w-1/2 sm:w-full">
      <label for="" class="block mb-2 text-sm font-medium text-black">Tokopedia Link</label>
      <input type="text" name="link_tokped" class="bg-gray-50 border border-gray-300 text-black text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5" value="{{$post->link_tokped}}"" required>
    </div>
    <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm sm:w-auto px-5 py-2.5 text-center m-2 mb-2">Tambah</button>
    <a href="/admin/marketing" class="text-black flex items-center p-2 text-bold w-24 hover:text-red-600">
        <svg class="w-4 h-auto" stroke="currentColor" stroke-width="1.5" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
            <path stroke-linecap="round" stroke-linejoin="round" d="M10.5 19.5L3 12m0 0l7.5-7.5M3 12h18"></path>
          </svg>
        Kembali
    </a>
  </form>
  
</div>

@endsection