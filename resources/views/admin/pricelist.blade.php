@extends('admin.templateadmin')
@section('content')



<div class="relative overflow-x-auto shadow-md sm:rounded-lg p-2 h-96 overflow-auto">
    <h4 class="text-2xl font-bold text-black mb-4 mt-2">Price List</h4>
    <div class="flex items-center justify-between pb-4">
        <div>
            <a href='/admin/pricelist/create'>
                <button type="button" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:ring-blue-300 md:font-medium font-xs rounded-lg text-sm px-5 py-2.5 mr-2 mb-2">+Upload Price List</button>
            </a>
        </div>
        <label for="table-search" class="sr-only">Search</label>
        <div class="relative">
            <div class="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
                <svg class="w-5 h-5 text-gray-500" aria-hidden="true" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z" clip-rule="evenodd"></path></svg>
            </div>
            <input type="text" id="table-search" class="block p-2 pl-10 text-sm text-gray-900 border border-gray-300 rounded-lg w-80 bg-gray-50 focus:ring-blue-300 focus:border-blue-500" placeholder="Cari Kategori">
        </div>
    </div>
    <table class="text-sm text-gray-500 border border-gray-300 w-full">
        <thead class="text-xs text-center text-gray-700 uppercase bg-gray-100 border border-gray-300 shadow">
            <tr>
                <th scope="col" class="px-6 py-3 ">
                    No
                </th>
                <th scope="col" class="px-6 py-3">
                    Tanggal
                </th>
                <th scope="col" class="px-6 py-3">
                    File
                </th>
                <th scope="col" class="px-6 py-3">
                    Status
                </th>
            </tr>
        </thead>
        <tbody>
            @foreach ($post as $no => $value)
            <tr class="bg-white border-b hover:bg-gray-50 ">
                <td class="w-4 p-4 text-center">
                    {{$no+1}}
                </td>
                <th scope="row" class="px-6 py-4 font-medium text-black">
                    {{$value->tanggal}}
                </th>
                <td class="px-6 py-4">
                    <a href="{{asset('upload/pricelist/'.$value->file)}}" target="blank">
                        <svg fill="none" class="w-5 h-5 m-auto" stroke="blue" stroke-width="2" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg" aria-hidden="true">
                            <path stroke-linecap="round" stroke-linejoin="round" d="M4.5 19.5l15-15m0 0H8.25m11.25 0v11.25"></path>
                        </svg>
                    </a>
                </td>
                <td class="px-6 py-4 text-center">
                    <?php $result = ($value->status == true)? "Aktif" : "Tidak Aktif"; echo $result;?>
                </td>
                {{-- <td class="px-6 py-4 text-center flex gap-2">
                    <form method="POST" action="/admin/pricelist/{{$value->id}}">
                        @method('DELETE')
                        @csrf
                        <button type="submit" class="text-white bg-red-700 hover:bg-red-800 focus:ring-4 focus:ring-red-300 font-medium rounded-lg text-sm w-24 px-5 py-2.5">Hapus</button>
                    </form>
                </td> --}}
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

@endsection