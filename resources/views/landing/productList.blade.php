@extends('landing.template')
@section('content')
<?php 
echo "<input type='text' id='marketingName' value=$name hidden>";
?>
<section id="#" class=" relative wow fadeInUp z-20  overflow-hidden bg-[#f3f4fe] pt-5 pb-5 ">
    <div class="container mx-auto mt-[80px]">
        <div class=" flex flex-wrap">
            <div class="w-full px-4">
                <div class="mx-auto max-w-[620px] text-center">
                    <h2 class="mb-4 text-2xl h-10 font-bold text-dark sm:text-4xl md:text-[42px] max-w-auto text-center">
                        Produk Kami
                    </h2>
                </div>
            </div>
        </div> 
            <ul id="listProduk" class="flex flex-nowrap text-xm font-medium text-center text-gray-500 border-b border-gray-200 px-5 w-full overflow-x-scroll">
                
            </ul>
        <!-- card -->
        
        <div class="produkList grid grid-cols-2 md:grid-cols-4 gap-4 p-10">
            @forelse ($post as $val)
            <div class="relative h-auto max-w-1/3 rounded-lg hover:scale-125 transition-all duration-500 cursor-pointer hover:z-20 ">
                <img class="" src="/upload/{{$val->foto}}">
                <div class="absolute inset-0 opacity-0 hover:opacity-100 p-2 m-2 md:m-5"> 
                    <a href="/{{$name}}/produk/{{$val->id}}" class="inline-flex items-center px-3 py-2 text-sm font-light md:text-center text-xs text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                        Read More
                        <svg aria-hidden="true" class="w-4 h-4 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    </a>
                </div>
                <p class="text-gray-500 dark:text-gray-400">
                    <a href="/{{$name}}/produk/{{$val->id}}" class="inline-flex items-center font-medium text-blue-600 dark:text-blue-500 hover:underline">
                        {{$val->nama_produk}}
                        <svg aria-hidden="true" class="w-5 h-5 ml-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M12.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-2.293-2.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                    </a>
                </p>
            </div>
            @empty
            <div class="mx-auto max-w-[620px] text-center">
                <h2 class="mb-4 text-2xl h-10 font-bold text-dark sm:text-4xl md:text-[42px] max-w-auto text-center italic">
                    Produk Kosong
                </h2>
            </div>
            @endforelse
        </div>

    </div>
    <!-- end card -->
</section>
@endsection

@push('script')
<script type="text/javascript">
$(document).ready(function(){
    marketing = $("#marketingName").val();
    $.ajax({
        type : "GET",
        url : "/"+marketing+"/getAll",
        data : [

        ],
        success : function(data) {
            data = $.parseJSON(data)
            list = $('#listProduk')
            if (data.length > 0) {
                list.html(`
                    <li class="mr-2 rounded-t-lg">
                    <a href="#" class="listKategori text-sm allProduct inline-block p-2 rounded-t-lg hover:bg-blue-400 hover:text-white bg-blue-400 text-white h-full flex items-center">All Product</a>
                    </li>
                `)
                $.each(data,function(i,val){
                list.append(`
                    <li class="mr-2 rounded-t-lg">
                    <a href="#" class="listKategori text-sm inline-block p-2 rounded-t-lg hover:bg-blue-400 hover:text-white h-full flex items-center">`+val['nama_kategori']+`</a>
                    </li>
                `)
                })
            }
            $('.allProduct').on('click',function(){
                produkList = $('.produkList')
                produkList.empty()
                $.ajax({
                    type : "GET",
                    url : "/"+marketing+"/getAllList",
                    data : {

                    },
                    success : function(data){
                        data = $.parseJSON(data)
                        $.each(data,function(i,val){
                            produkList.append(`
                            <div class="relative h-auto max-w-1/3 rounded-lg hover:scale-125 transition-all duration-500 cursor-pointer hover:brightness-50 hover:z-20 ">
                            <img class="" src="/upload/`+val.foto+`">
                            <div class="absolute inset-0 opacity-0 hover:opacity-100 p-2 m-2 md:m-5"> 
                            <h1 class="mb-2 md:mb-10 text-3xl font-extrabold text-gray-900 text-sm md:text-3xl"><span class="text-transparent bg-clip-text bg-gradient-to-r to-emerald-500 from-sky-400">`+val.nama_produk+`</span></h1>

                            <a href=""/`+marketing+`/produk/`+val.id+`" class="inline-flex items-center px-3 py-2 text-sm font-light md:text-center text-xs text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                            Read More
                            <svg aria-hidden="true" class="w-4 h-4 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            </a>
                            </div>
                            <p class="text-gray-500 dark:text-gray-400">
                            <a href="/`+marketing+`/produk/`+val.id+`" class="inline-flex items-center font-medium text-blue-600 dark:text-blue-500 hover:underline">
                            `+val.nama_produk+`
                            <svg aria-hidden="true" class="w-5 h-5 ml-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M12.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-2.293-2.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            </a>
                            </p>
                            </div>
                            `)
                        })
                    }
                })
            })

            $('.listKategori').on('click',function(){
                $('.listKategori').removeClass('bg-blue-400 text-white')
                $(this).addClass('bg-blue-400 text-white')
                produk = $(this).text()
                if(produk != "All Product"){
                $.ajax({
                    type : "GET",
                    url : "/"+marketing+"/getList/"+ $(this).text(),
                    data : {
                    },
                    success : function(data){
                        data = $.parseJSON(data)
                        produkList = $('.produkList')
                        produkList.empty()
                        $.each(data, function(i,val){
                            produkList.append(`
                            <div class="relative h-auto max-w-1/3 rounded-lg hover:scale-125 transition-all duration-500 cursor-pointer hover:brightness-50 hover:z-20 ">
                            <img class="" src="/upload/`+val.foto+`">
                            <div class="absolute inset-0 opacity-0 hover:opacity-100 p-2 m-2 md:m-5"> 
                            <h1 class="mb-2 md:mb-10 text-3xl font-extrabold text-gray-900 text-sm md:text-3xl"><span class="text-transparent bg-clip-text bg-gradient-to-r to-emerald-500 from-sky-400">`+val.nama_produk+`</span></h1>

                            <a href="/`+marketing+`/produk/`+val.id+`" class="inline-flex items-center px-3 py-2 text-sm font-light md:text-center text-xs text-white bg-blue-700 rounded-lg hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">
                            Read More
                            <svg aria-hidden="true" class="w-4 h-4 ml-2 -mr-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            </a>
                            </div>
                            <p class="text-gray-500 dark:text-gray-400">
                            <a href="/`+marketing+`/produk/`+val.id+`" class="inline-flex items-center font-medium text-blue-600 dark:text-blue-500 hover:underline">
                            `+val.nama_produk+`
                            <svg aria-hidden="true" class="w-5 h-5 ml-1" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><path fill-rule="evenodd" d="M12.293 5.293a1 1 0 011.414 0l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-2.293-2.293a1 1 0 010-1.414z" clip-rule="evenodd"></path></svg>
                            </a>
                            </p>
                            </div>
                            
                            `)
                        })
                    }
                })
            }
            })
        }
    })
})

</script>
@endpush
