<!doctype html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    @vite(['resources/css/app.css','resources/js/app.js'])
    <link rel="stylesheet" href="{{asset('css/animate.css')}}">
    <link rel="stylesheet" href="{{asset('css/tailwind.css')}}" />
    <title>Metro Display ADV</title>
    <link rel="shortcut icon" href="{{asset('assets/images/logo/logo-putih.png')}}" type="image/x-icon" />
    <script src="{{asset('js/tailwindcss.js')}}"></script>
    <script src="{{asset('js/wow.min.js')}}"></script>
    <script src="https://code.jquery.com/jquery-3.6.4.js"> </script>

    <script>
        new WOW().init();
    </script>
    @stack('style')
</head>

<body>
    WEY !
</body>
<script src="https://unpkg.com/flowbite@1.4.0/dist/flowbite.js"></script>
@stack('script')
</html>