<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\user\landingPageController;
use App\Http\Controllers\user\landingPageProdukController;
use App\Http\Controllers\user\landingPageListProdukController;
use App\Http\Controllers\user\customerController;


use App\Http\Controllers\admin\marketingController;
use App\Http\Controllers\admin\adminController;
use App\Http\Controllers\admin\kategoriController;
use App\Http\Controllers\admin\produkController;
use App\Http\Controllers\admin\userController;
use App\Http\Controllers\admin\bannerController;
use App\Http\Controllers\admin\loginController;
use App\Http\Controllers\admin\priceListController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//admin
Route::resource('admin/dashboard', adminController::class)->middleware('isAdmin');
Route::resource('admin/marketing', marketingController::class)->middleware('isAdmin');
Route::resource('admin/produk', produkController::class)->middleware('isAdmin');
Route::resource('admin/kategori', kategoriController::class)->middleware('isAdmin');
Route::resource('admin/users', userController::class)->middleware('isAdmin');
Route::resource('admin/banner', bannerController::class)->middleware('isAdmin');
Route::resource('admin/pricelist', priceListController::class)->middleware('isAdmin');
Route::get('admin/produk/photo/{id}/{list_foto}', [produkController::class, 'deletePhoto'])->middleware('isAdmin');
Route::get('admin/getvisitors', [adminController::class, 'getVisitors'])->middleware('isAdmin');
Route::get('admin/getallmarketing', [marketingController::class, 'getAllMarketing'])->middleware('isAdmin');

//Auth
Route::get('login', [loginController::class, 'index'])->name('login')->middleware('guest');
Route::post('login', [loginController::class, 'auth'])->middleware('guest');
Route::post('logout', [loginController::class, 'logout'])->middleware('isAdmin');


//landing page
Route::middleware('visitor')->group(function(){
    Route::get('/', [landingPageController::class, 'landingPageUniversal']);
    Route::get('/{name}', [landingPageController::class, 'landingPageUniversal'])->middleware('isMarketing');
    Route::resource('/{name}/listproduk', landingPageListProdukController::class)->middleware('isMarketing');
    Route::resource('/{name}/produk', landingPageProdukController::class)->middleware('isMarketing');
    Route::resource('/{name}/customer', customerController::class)->middleware('isMarketing');
    Route::get('/{name}/getList/{nama_kategori}', [landingPageListProdukController::class, 'getFilterList'])->middleware('isMarketing');
    Route::get('/{name}/getAllList', [landingPageListProdukController::class, 'getAllList'])->middleware('isMarketing');
    Route::get('/{name}/getAll', [landingPageListProdukController::class, 'getAllKategori'])->middleware('isMarketing');
    Route::get('/{name}/pricelist', [priceListController::class, 'getPriceList'])->middleware('isMarketing');
});