<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\user\landingPageController;
use Symfony\Component\HttpFoundation\Cookie;


class visitor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $visitors = request()->cookie('visitors');

        $addVisitor = new landingPageController();
        $name = $request->route('name');

        if (!$visitors) {
            $cookie = cookie('visitors', true, 120);
            $addVisitor->storeVisitor($name);

            return $next($request)->cookie($cookie);
        }
        return $next($request);
    }
}
