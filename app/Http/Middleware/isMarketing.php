<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use App\Http\Controllers\admin\marketingController;

class isMarketing
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $marketing = new marketingController();
        $name = $request->route('name');
        $data = $marketing->getMarketing($name);

        if ($data != "[]" || $name == 'universal') {
            return $next($request);
        }
        return redirect('/');
    }
}
