<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class produkController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = DB::table('produk')->leftjoin('kategori_produk', 'produk.kategori_id', '=', 'kategori_produk.id')->select('*', 'produk.id as produk_id')->get();
        return view('admin.produk', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $post = DB::table('kategori_produk')->get();
        return view('admin.tambahProduk', compact('post'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $upload_foto = array();
        $tujuan = 'upload';
        if ($input = $request->upload_foto) {
            foreach ($input as $upload) {
                $nama_file = time() . "_" . $upload->getClientOriginalName();
                $upload->move($tujuan, $nama_file);
                $upload_foto[] = $nama_file;
            }
        }

        $upload_detail = array();
        $tujuan = 'upload/detail';
        if ($input = $request->detail_foto) {
            foreach ($input as $upload) {
                $nama_file = time() . "_" . $upload->getClientOriginalName();;
                $upload->move($tujuan, $nama_file);
                $upload_detail[] = $nama_file;
            }
        }

        $harga = str_replace(',','',$request->harga);
        $post = DB::table('produk')->insert([
            'nama_produk' => $request->nama_produk,
            'sistem_operasi' => $request->sistem_operasi,
            'ukuran' => $request->ukuran,
            'harga' => $harga,
            'kategori_id' => $request->kategori_id,
            'upload_foto' => implode(",", $upload_foto),
            'detail_foto' => implode(",", $upload_detail),
            'lain_lain' => $request->lain_lain,
            'spesifikasi' => $request->spesifikasi,
        ]);
        return redirect('/admin/produk');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = DB::table('produk')->find($id);
        return json_encode($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($produk_id)
    {
        $post = DB::table('produk')->leftjoin('kategori_produk', 'produk.kategori_id', '=', 'kategori_produk.id')->select('*', 'produk.id as produk_id','nama_kategori')->where('produk.id', $produk_id)->get();
        $postKategori = DB::table('kategori_produk')->get();
        return view('admin/editProduk', compact('post'))->with(compact('postKategori'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $upload_foto = array();
        $tujuan = 'upload';
        if ($input = $request->upload_foto) {
            foreach ($input as $upload) {
                $nama_file = time() . "_" . $upload->getClientOriginalName();;
                $upload->move($tujuan, $nama_file);
                $upload_foto[] = $nama_file;
            }
        }

        if ($request->list_foto != null) {
            $upload_foto= array_merge($upload_foto, $request->list_foto);
        }

        $upload_detail = array();
        $tujuan = 'upload/detail';
        if ($input = $request->upload_detail) {
            foreach ($input as $upload) {
                $nama_file = time() . "_" . $upload->getClientOriginalName();;
                $upload->move($tujuan, $nama_file);
                $upload_detail[] = $nama_file;
            }
        }

        if ($request->list_foto_detail != null) {
            $upload_detail= array_merge($upload_detail, $request->list_foto_detail);
        }

        $harga = str_replace(',','',$request->harga);
        DB::table('produk')->where('id', $id)->update([
            'nama_produk' => $request->nama_produk,
            'sistem_operasi' => $request->sistem_operasi,
            'ukuran' => $request->ukuran,
            'harga' => $harga,
            'kategori_id' => $request->kategori_id,
            'upload_foto' => implode(",", $upload_foto),
            'detail_foto' => implode(",", $upload_detail),
            'lain_lain' => $request->lain_lain,
            'spesifikasi' => $request->spesifikasi,
        ]);
        return redirect('/admin/produk');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('produk')->where('id', $id)->delete();
        return redirect('/admin/produk');
    }
    public function deletePhoto($id, $list_foto){
        $post = DB::table('produk')->find($id);
        $foto = explode(',', $post->upload_foto);

        $foto = array_diff($foto, [$list_foto]);

        $newUploadFoto = implode(", ", $foto);

        DB::table('produk')->where('id', $id)->update([
            'upload_foto' => $newUploadFoto
        ]);
        return redirect()->back()->withInput();
    }
}
