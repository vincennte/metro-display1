<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class adminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk = DB::table('produk')->get();
        $kategori = DB::table('kategori_produk')->leftjoin('produk', 'kategori_id', '=', 'kategori_produk.id')->select(DB::raw('COUNT(produk.kategori_id) AS total'), 'nama_kategori')->groupby('nama_kategori')->orderby('total', 'DESC')->get();
        $visitors = DB::table('pengunjung')->select(DB::raw("SUM(jumlah_pengunjung) as result"))->get();
        $pesan = DB::table('customer')->get();
        
        return view('admin.index')->with(compact('produk'))->with(compact('kategori'))->with(compact('pesan'))->with(compact('visitors'));

    }
    public function getVisitors(){
        $getVisitors = DB::table(DB::raw('(SELECT 1 AS month UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9 UNION SELECT 10 UNION SELECT 11 UNION SELECT 12) as months'))->leftJoin('pengunjung as p', function ($join) {$join->on('months.month', '=', 'p.bulan')->where('p.tahun', '=', date('Y'));})->select('months.month as bulan', DB::raw('COALESCE(p.jumlah_pengunjung, 0) as jumlah_pengunjung'))->orderBy('months.month')->get()->toArray();

        $result = [];
        for($i=0;$i<count($getVisitors);$i++){
            array_push($result, $getVisitors[$i]->jumlah_pengunjung);
        }

        return $result;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
