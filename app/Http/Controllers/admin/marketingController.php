<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class marketingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = DB::table('user_marketing')->get();
        return view('admin.marketing', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambahMarketing');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('user_marketing')->insert([
            'username' => $request->username,
            'no_hp' => $request->no_hp,
            'nama_lengkap' => $request->nama_lengkap,
            'link_tokped' => $request->link_tokped
        ]);
        return redirect('/admin/marketing');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getMarketing($username)
    {
        $post = DB::table('user_marketing')->where('username', $username)->get();
        return $post;
    }
    public function getAllMarketing()
    {
        $post = DB::table('user_marketing')->get();
        return $post;
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = DB::table('user_marketing')->find($id);
        return view('admin.editMarketing', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = DB::table('user_marketing')->where('id', $id)->update([
            'no_hp' => $request->no_hp,
            'nama_lengkap' => $request->nama_lengkap,
            'link_tokped' => $request->link_tokped
        ]);
        return redirect('/admin/marketing');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = DB::table('user_marketing')->where('id', $id)->delete();
        return redirect('/admin/marketing');
    }
}
