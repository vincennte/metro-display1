<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class bannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = DB::table('banner')->orderBy("status", "DESC")->get();

        return view('admin.banner', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambahBanner');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $upload= $request->upload_banner;
        $tujuan = 'upload/banner';
        $nama_file = time() . "_" . $upload->getClientOriginalName();
        $upload->move($tujuan, $nama_file);
        $upload_banner = $nama_file;

        $post = DB::table('banner')->insert([
            'nama_banner' => $request->nama_banner,
            'upload_banner' => $upload_banner,
            'status' => $request->status,
            'link' => $request->link,
        ]);
        return redirect('/admin/banner');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = DB::table('banner')->find($id);
        return json_encode($post);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $upload= $request->upload_banner;
        if ($upload !== null){
            $tujuan = 'upload/banner';
            $nama_file = time() . "_" . $upload->getClientOriginalName();
            $upload->move($tujuan, $nama_file);
            $upload_banner = $nama_file;

            DB::table('banner')->where('id', $id)->update([
                'nama_banner' => $request->nama_banner,
                'upload_banner' => $upload_banner,
                'status' => $request->status,
                'link' => $request->link,
            ]);
        }
        else {
            DB::table('banner')->where('id', $id)->update([
                'nama_banner' => $request->nama_banner,
                'status' => $request->status,
                'link' => $request->link,
            ]);
        }
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
