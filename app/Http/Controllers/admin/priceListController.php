<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use DB;

class priceListController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $post = DB::table('pricelist')->orderBy('status', 'desc')->get();
        return view('admin.pricelist', compact('post'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tambahPriceList');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $update = DB::table('pricelist')->where('status', true)->update([
            'status' => false,
        ]);

        $tujuan = 'upload/pricelist';
        $input = $request->file;

        $nama_file = time() . "_" . $input->getClientOriginalName();
        $input->move($tujuan, $nama_file);
    
        $post = DB::table('pricelist')->insert([
            'tanggal' => now(),
            'file' => $nama_file,
            'status' => true,
        ]);
        return redirect('/admin/pricelist');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $post = DB::table('pricelist')->where('status', true)->first();
        $fileLink = asset('upload/pricelist' . $post->id);
        return $fileLink;
        
    }
    public function getPriceList()
    {
        $post = DB::table('pricelist')->where('status', true)->first();
        $fileLink = asset('upload/pricelist/' . $post->file);
        return $fileLink;
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        
    }
}
