<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Controllers\admin\marketingController;
use DB;

class landingPageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('landing.landingPage');
    }
    public function landingPageUniversal($name = "universal")
    {
        $post = DB::table('banner')->where("status",true)->orderBy("status", "DESC")->get();
        
        $marketing = new marketingController();
        $data = json_decode($marketing->getMarketing($name),true);
        
        $tokped = $data[0]['link_tokped'];
        $no_hp = $data[0]['no_hp'];

        return view('landing.landingPage', compact('post'), ['name' => $name])->with('tokped', $tokped)->with('no_hp', $no_hp);
    }

    public function storeVisitor($name){

        $marketing = new marketingController();
        $marketing = json_decode($marketing->getMarketing($name),true);

        if($name == null || $marketing == []){
            $name = "universal";
        }

        $bulan = date('m');
        $tahun = date('Y');
        $jmlPengunjung = 1 ;
        $getJumlahPengunjung = DB::table('pengunjung')->where("bulan",$bulan)->where("tahun",$tahun)->first();

        if($getJumlahPengunjung){
            $jmlPengunjung = $getJumlahPengunjung->jumlah_pengunjung + 1;
        }
        $storeVisitor = DB::table('pengunjung')->updateOrInsert(['bulan' => $bulan, 'tahun' => $tahun],[
            'jumlah_pengunjung' => $jmlPengunjung,
            'bulan' => $bulan,
            'tahun' => $tahun,
        ]);

        $getMarketing = DB::table('user_marketing')->where('username', $name)->first();
        $storeMarketing = DB::table('user_marketing')->where('username', $getMarketing->username)->update([
            'pengunjung' => $getMarketing->pengunjung + 1,
        ]);
        return;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
