<?php

namespace App\Http\Controllers\user;

use App\Http\Controllers\Controller;
use App\Http\Controllers\admin\marketingController;
use Illuminate\Http\Request;
use DB;

class landingPageListProdukController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($name)
    {
        $post = DB::table('produk')->select(DB::raw("id,nama_produk, SUBSTRING_INDEX(upload_foto, ',', 1) as foto"))->get();

        $marketing = new marketingController();
        $data = json_decode($marketing->getMarketing($name),true);
        $tokped = $data[0]['link_tokped'];
        $no_hp = $data[0]['no_hp'];

        return view('landing.productList', compact('post'), ['name' => $name])->with('tokped', $tokped)->with('no_hp', $no_hp);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }
    public function getFilterList($nama, $nama_kategori)
    {
        $post = DB::table('produk')->leftjoin('kategori_produk', 'produk.kategori_id', '=', 'kategori_produk.id')->select(DB::raw("produk.id,kategori_produk.nama_kategori,nama_produk, SUBSTRING_INDEX(upload_foto, ',', 1) as foto"))->where('nama_kategori', $nama_kategori)->get();
        return json_encode($post);
    }
    public function getAllKategori()
    {
        $kategori = DB::table('kategori_produk')->get();
        return json_encode($kategori);
    }
    public function getAllList()
    {
        $post = DB::table('produk')->leftjoin('kategori_produk', 'produk.kategori_id', '=', 'kategori_produk.id')->select(DB::raw("produk.id,kategori_produk.nama_kategori,nama_produk, SUBSTRING_INDEX(upload_foto, ',', 1) as foto"))->get();
        return json_encode($post);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
